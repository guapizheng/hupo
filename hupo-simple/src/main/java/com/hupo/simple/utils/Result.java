package com.hupo.simple.utils;

import lombok.Data;
import lombok.Getter;
import lombok.Setter;
import org.apache.commons.lang3.StringUtils;

/**
 * @description：结果统一封装
 * @version: $
 */
@Data
public class Result<T> {

    public Result(String code, String message, T data, Long timeStamp) {
        this.code = code;
        this.message = message;
        this.data = data;
        this.timeStamp = timeStamp;
    }

    private String code;
    private String message;
    private T data;
    private Long timeStamp;


    public static <T> Result<T> success(T data) {
        return new Result<T>(ResultCode.SUCCESS.getCode(), ResultCode.SUCCESS.getMessage(), data, System.currentTimeMillis());
    }
}

