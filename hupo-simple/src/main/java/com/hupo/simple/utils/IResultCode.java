package com.hupo.simple.utils;

import java.util.EnumSet;
import java.util.Iterator;

/**
 * 枚举的公共属性
 *
 * @author LiuKui
 * @since 1.0.0
 */
public interface IResultCode {

    String getCode();

    String getMessage();

    /**
     * 获取枚举类
     * @param code code值
     * @param clz 类型名称
     * @param <E> 枚举
     * @return 获取枚举类
     */
     static<E extends Enum<E> & IResultCode> E getEnum(Class<E> clz,String code){
         if(code != null) {
             EnumSet<E> es = EnumSet.allOf(clz);
             Iterator<E> it = es.iterator();
             while (it.hasNext()){
                 E item = it.next();
                 if(item.getCode().equals(code)){
                     return item;
                 }
             }
         }
        return null;
    }

}
