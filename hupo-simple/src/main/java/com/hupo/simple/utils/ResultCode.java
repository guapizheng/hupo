package com.hupo.simple.utils;

import com.fasterxml.jackson.annotation.JsonFormat;
import lombok.AccessLevel;
import lombok.AllArgsConstructor;
import lombok.Getter;

@Getter
@JsonFormat(shape = JsonFormat.Shape.OBJECT)
@AllArgsConstructor(access = AccessLevel.PRIVATE)
public enum ResultCode implements IResultCode{

    SUCCESS("00000", "请求成功"),
    FAILURE("-1", "请求失败"),
    EMPTY_PARAM("A0410", "请求必填参数为空"),
    SERVICE_ERROR("50000", "服务器去旅行了，请稍后重试"),
    PARAM_INVALID("50001", "无效的参数"),
    ;

    private final String code;
    private final String message;

}

