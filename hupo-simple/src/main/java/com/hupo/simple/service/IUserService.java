package com.hupo.simple.service;

import com.baomidou.mybatisplus.extension.service.IService;
import com.hupo.simple.model.dto.UserInfoDTO;
import com.hupo.simple.model.po.UserInfo;
import com.hupo.simple.model.req.UserInfoReq;

import java.util.List;

/**
 * @author dell
 * @description 针对表【user】的数据库操作Service
 * @createDate 2024-01-18 16:07:07
 */
public interface IUserService extends IService<UserInfo> {

    /**
     * 自定义查询
     *
     * @param id 入参
     * @return UserInfo
     * @author 琥珀
     * @date 2024/4/7
     **/
    UserInfo getUserInfo(Long id);

    /**
     * 查询用户详情列表
     *
     * @param infoReq UserInfoReq
     * @param desc    入参
     * @return List<UserInfo>
     * @author 琥珀
     * @date 2024/5/10
     **/
    List<UserInfoDTO> userInfoList(UserInfoReq infoReq, String desc);
}
