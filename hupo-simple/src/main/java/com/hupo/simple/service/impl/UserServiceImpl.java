package com.hupo.simple.service.impl;

import com.alibaba.fastjson.JSON;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.hupo.simple.mapper.UserMapper;
import com.hupo.simple.model.dto.UserInfoDTO;
import com.hupo.simple.model.po.UserInfo;
import com.hupo.simple.model.req.UserInfoReq;
import com.hupo.simple.service.IUserService;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.stream.Collectors;

/**
 * @author dell
 * @description 针对表【user】的数据库操作Service实现
 * @createDate 2024-01-18 16:07:07
 */
@Service
@Slf4j
@RequiredArgsConstructor
public class UserServiceImpl extends ServiceImpl<UserMapper, UserInfo> implements IUserService {

    private final UserMapper userMapper;

    @Override
    public UserInfo getUserInfo(Long id) {
        UserInfo userInfo = userMapper.getUserInfo(id);
        log.info("用户查询 >> getUserInfo:{}", JSON.toJSONString(userInfo));
        return userInfo;
    }

    @Override
    public List<UserInfoDTO> userInfoList(UserInfoReq infoReq, String desc) {
        List<UserInfo> userInfoList = userMapper.getUserInfoList(infoReq, desc);
       return userInfoList.stream().map(userInfo -> UserInfoDTO.builder().createUser(userInfo.getCreateUser())
               .address(userInfo.getAddress())
               .age(userInfo.getAge())
               .userName(userInfo.getUserName()).build()).collect(Collectors.toList());
    }
}
