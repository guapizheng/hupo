package com.hupo.simple.model.po;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import com.fasterxml.jackson.annotation.JsonFormat;
import com.hupo.simple.constants.DateFormatConstant;
import lombok.*;
import org.springframework.format.annotation.DateTimeFormat;

import java.io.Serializable;
import java.time.LocalDateTime;

/**
 * @TableName user
 * MybatisPlus 表名为Mysql、Postgresql关键字如何处理
 */
@TableName("\"user\"")
@Data
@NoArgsConstructor
@AllArgsConstructor
public class UserInfo implements Serializable {

    private static final long serialVersionUID = -5999163478381083358L;

    @TableId(value = "id", type = IdType.ASSIGN_ID)
    private Long id;

    private String userName;

    private Integer age;

    private String address;

    /**
     * @DateTimeFormat: 页面将数据传到后台，是以字符串的形式。所以时间格式会出错。加上此注解，后台可解析时间格式的字符串
     * @JsonFormat: 后台传到前台，前台没办法解析,所以加上此字段
     * @JsonFormat(pattern=”yyyy-MM-dd”,timezone=”GMT+8”)：因为我们是东八区（北京时间）。所以我们在格式化的时候要指定时区（timezone ）
     */
    @DateTimeFormat(pattern = DateFormatConstant.DATE_FORMAT_WITH_SECOND)
    @JsonFormat(pattern = DateFormatConstant.DATE_FORMAT_WITH_SECOND)
    private LocalDateTime createTime;

    private String createUser;

    @DateTimeFormat(pattern = DateFormatConstant.DATE_FORMAT_WITH_SECOND)
    @JsonFormat(pattern = DateFormatConstant.DATE_FORMAT_WITH_SECOND)
    private LocalDateTime updateTime;

    private String updateUser;

    private Integer deleted;

}