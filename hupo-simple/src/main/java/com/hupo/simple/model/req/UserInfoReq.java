package com.hupo.simple.model.req;

import lombok.Data;

import javax.validation.constraints.NotBlank;
import java.io.Serializable;

@Data
public class UserInfoReq implements Serializable {
    private static final long serialVersionUID = -6724578600276558371L;

    @NotBlank(message = "用户昵称不能为空")
    private String userName;
    private Long id;
}
