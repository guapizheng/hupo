package com.hupo.simple.model.dto;

import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.*;
import lombok.experimental.Accessors;
import org.apache.commons.lang3.StringUtils;

@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
@Accessors(chain = true)
@EqualsAndHashCode( callSuper = false)
public class UserInfoDTO {

    private String createUser;
    private String userName;
    private Integer age;
    private String address;

    @JsonProperty()
    private Boolean isOwned() {
        return StringUtils.isNotBlank(createUser);
    }
}
