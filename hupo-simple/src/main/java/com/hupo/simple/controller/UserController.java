package com.hupo.simple.controller;

import com.hupo.simple.model.dto.UserInfoDTO;
import com.hupo.simple.model.po.UserInfo;
import com.hupo.simple.model.req.UserInfoReq;
import com.hupo.simple.service.IUserService;
import com.hupo.simple.utils.Result;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

import javax.annotation.Resource;
import javax.validation.Valid;
import java.util.List;

@RestController
public class UserController {

    @Resource
    private IUserService userService;

    @PostMapping("add")
    public Result<Boolean> add(@RequestBody UserInfo user) {
        return Result.success(userService.save(user));
    }

    @GetMapping("get")
    public Result<UserInfo> queryUserInfo(Long id) {
        return Result.success(userService.getUserInfo(id));
    }

    @PostMapping("userinfo")
    public Result<List<UserInfoDTO>> userInfoList(@Valid @RequestBody UserInfoReq infoReq, String desc) {
        return Result.success(userService.userInfoList(infoReq, desc));
    }

}
