package com.hupo.simple.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.hupo.simple.model.po.UserInfo;
import com.hupo.simple.model.req.UserInfoReq;
import org.apache.ibatis.annotations.Param;

import java.util.List;

public interface UserMapper extends BaseMapper<UserInfo> {

    /**
     * 根据id查询用户详情
     *
     * @param id 主键id
     * @return UserInfo
     * @author 琥珀
     * @date 2024/4/7
     **/
    UserInfo getUserInfo(@Param("id") Long id);

    /**
     * 查询用户列表
     *
     * @param infoReq
     * @param desc    入参
     * @return List<UserInfo>
     * @author 琥珀
     * @date 2024/10/14
     **/

    List<UserInfo> getUserInfoList(@Param("infoReq") UserInfoReq infoReq, @Param("desc") String desc);
}
