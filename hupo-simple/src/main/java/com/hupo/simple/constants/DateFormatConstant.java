package com.hupo.simple.constants;

/**
 * 时间格式化常量
 */
public interface DateFormatConstant {
    String DATE_FORMAT_WITH_MONTH = "yyyy-MM";
    String DATE_FORMAT_WITH_DATE = "yyyy-MM-dd";
    String DATE_FORMAT_WITH_HOUR = "yyyy-MM-dd HH";
    String DATE_FORMAT_WITH_MINUTE = "yyyy-MM-dd HH:mm";
    String DATE_FORMAT_WITH_SECOND = "yyyy-MM-dd HH:mm:ss";
    String DATE_FORMAT_WITH_MILLISECONDS = "yyyy-MM-dd HH:mm:ss:SSS";
}
